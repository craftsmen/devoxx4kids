# Devoxx4Kids



[Minecraft](https://gitlab.com/craftsmen/devoxx4kids/-/tree/master/Minecraft)
- Opdracht | [Minecraft aanpassen met behulp van Forge](https://gitlab.com/craftsmen/devoxx4kids/-/blob/master/Minecraft/Minecraft_modding_Kadaster_Craftsmen_10-2024.pdf)

[mBot](https://gitlab.com/craftsmen/devoxx4kids/-/tree/master/mBot) 
- Opdracht 1 | [Richtingaanwijzers maken](https://gitlab.com/craftsmen/devoxx4kids/-/blob/master/mBot/01_Richtingaanwijzers_Maken.pdf) | [Uitgewerkte code](https://gitlab.com/craftsmen/devoxx4kids/-/blob/master/mBot/01_Richtingaanwijzers_Maken.sb2)
- Opdracht 2 | [Jij bestuurt de robot](https://gitlab.com/craftsmen/devoxx4kids/-/blob/master/mBot/02_Jij_Bestuurt_de_Robot.pdf) | [Uitgewerkte code](https://gitlab.com/craftsmen/devoxx4kids/-/blob/master/mBot/02_Jij_Bestuurt_de_Robot.sb2)
- Opdracht 3 | [De robot kan zelf sturen!](https://gitlab.com/craftsmen/devoxx4kids/-/blob/master/mBot/03_De_Robot_Kan_Zelf_Sturen.pdf) | [Uitgewerkte code](https://gitlab.com/craftsmen/devoxx4kids/-/blob/master/mBot/03_De_Robot_Kan_Zelf_Sturen.sb2)

[Micro:bit](https://gitlab.com/craftsmen/devoxx4kids/-/tree/master/MicroBit) 
- Opdracht | [Maak Pong met de micro:bit](https://gitlab.com/craftsmen/devoxx4kids/-/blob/master/MicroBit/Micro_bit_-_Pong_-_2024.pdf)
